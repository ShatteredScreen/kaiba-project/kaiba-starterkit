const glob = require('glob')
const path = require('path')
const PrerenderSpaPlugin = require('prerender-spa-plugin')
const Renderer = PrerenderSpaPlugin.PuppeteerRenderer

module.exports = (options, req) => ({
  entry: path.join(__dirname, '/src/index.js'),
  devServer: {
    open: true
  },

  /**
   * Add plugins to the webpack configuration
   * 
   * @param   {Object} config Chained webpack configuration
   * @returns {Object} An updated chained webpack configuration
   */
  webpack(config) {
    if (process.env.NODE_ENV === 'production') {      
      config.plugins.push(this.prerenderPlugin())
    }
    return config
  },

  /**
   * Extend the module rules for webpack
   * 
   * @param   {Object} config Chained webpack configuration
   * @returns {Object} An updated chained webpack configuration
   */
  extendWebpack(config) {
    // Add Markdown
    config.module.rule('markdown')
      .test(/\.md$/)
      .use('raw')
        .loader(require.resolve('raw-loader')) 
        .end()

    return config
  },

  /**
   * Build an array of routes that the prerenderer will build
   * 
   * @returns {Array} An array of routes
   */
  buildRoutes() {
    // Match content files from the content dir
    let matchedFiles = glob.sync('**/*.md', {
      cwd : path.join(__dirname, '/src/content'),
    })
    
    // Loop through the content files and build routes
    let routes = []
    for (let file of matchedFiles) {
      // Clean up the filename in the route
      let splitFilePath = file.split('/')
      let filename = splitFilePath
          .pop()
          .replace(/_/, '')
          .replace(/\.[^.]*$/, '')
      splitFilePath.push(filename)

      // Add it back in
      let route = '/' + splitFilePath.join('/')
      routes.push(route)
    }

    return routes
  },

  /**
   * Define the configuration for the prerenderer
   * @returns {PrerenderSpaPlugin} Configured webpack plugin instance
   */
  prerenderPlugin() {
    return new PrerenderSpaPlugin({
      // Prerender Settings
      staticDir: path.join(__dirname, '/dist'),
      routes: this.buildRoutes()
    })
  }
})