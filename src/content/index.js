/**
 * @file This file will be used to import all the content modules
 * @since 1.0.0
 * @author Tristan Chatman
 */

// Load the content into an object
let content = new Map()
let req = require.context('./', true, /.(md|txt)$/)
for (let key of req.keys()) {
  content.set(key, req(key))
}

// Exports
export default content