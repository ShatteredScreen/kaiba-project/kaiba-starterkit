/**
 * @file The main javascript file of the website.
 * @since 1.0.0
 * @author Tristan Chatman
 */

// Imports
import Vue from 'vue'
import { Kaiba } from 'kaiba'
import theme from 'kaiba-theme'

import SiteRoot from '@/SiteRoot'
import content from '@/content'

// Configure Kaiba
let kaiba = new Kaiba(content, theme)
Vue.use(kaiba)

// Init the site on load
let app = new Vue({
  el: "#app",
  router: kaiba.router,
  render: h => h(SiteRoot),
  mounted() {
    document.dispatchEvent(new Event("render-event"));
  }
});